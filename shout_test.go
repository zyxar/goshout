package shout

import (
    "errors"
    "fmt"
    "testing"
)

var handle *Shout
var err error

const (
    password = "hackme"
)

func init() {
    handle, err = New()
    if err != nil {
        panic("Error creating shout handle.\n")
    }
}

func quit() {
    handle.Destroy()
    Shutdown()
}

func TestVersion(t *testing.T) {
    s, a, b, c := Version()
    if s == "" || a == 0 {
        t.Errorf("Error on getting version.\n")
    } else {
        fmt.Printf("version string: %s\n", s)
        fmt.Printf("version integr: %d.%d.%d\n", a, b, c)
    }
}

func TestConn(t *testing.T) {
    err = func() error {
        err := handle.SetFormat(SHOUT_FORMAT_OGG)
        if err != nil {
            return fmt.Errorf("Error on setting format: %v.\n", err)
        }
        err = handle.SetPassword("hackme")
        if err != nil {
            return fmt.Errorf("Error on setting password: %v.\n", err)
        }
        handle.SetMount("/test")
        //handle.SetNonblocking(true)
        err = handle.Open()
        if err != nil {
            return fmt.Errorf("Error on opening: %v.\n", err)
        }
        err = handle.Close()
        if err != nil {
            return fmt.Errorf("Error on closing: %v.\n", err)
        }
        return nil
    }()
    if err != nil {
        t.Errorf("%v", err)
    }
}

func TestParam(t *testing.T) {
    err := func() error {
        a := handle.GetHost()
        if a != "localhost" {
            return errors.New("Error on getting host.\n")
        }
        b := handle.GetPort()
        if b != uint16(8000) {
            return errors.New("Error on getting port.\n")
        }
        a = handle.GetPassword()
        if a != password {
            return errors.New("Error on getting password.\n")
        }
        a = handle.GetMount()
        if a != "/test" {
            return errors.New("Error on getting mount.\n")
        }
        c := handle.GetFormat()
        if c != SHOUT_FORMAT_OGG {
            return errors.New("Error on getting format.\n")
        }
        return nil
    }()
    if err != nil {
        t.Errorf("%v", err)
    }
}

func TestFinished(t *testing.T) {
    quit()
}
